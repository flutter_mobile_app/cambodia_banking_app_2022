import 'package:cambodia_banking_app/screens/authentication/login_screen.dart';
import 'package:cambodia_banking_app/screens/home/home_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:splashscreen/splashscreen.dart';

class SplashScreenApp extends StatefulWidget {
  const SplashScreenApp({Key? key}) : super(key: key);

  @override
  State<SplashScreenApp> createState() => _SplashScreenAppState();
}

class _SplashScreenAppState extends State<SplashScreenApp> {
  final localStorage = LocalStorage("TOKEN_APP");

  @override
  Widget build(BuildContext context) {
    return  SplashScreen(
      seconds: 1,
      navigateAfterSeconds: FutureBuilder(
        future: localStorage.ready,
        builder: (BuildContext context, data) {
          var token = localStorage.getItem("ACCESS_TOKEN");
          if (token != null) {
            return const HomeScreen();
          }
          return const LoginScreen();
        },
      ),
      image: Image.asset("assets/logo/logo.png"),
      photoSize: 100,
      title: Text("Mobile Banking App"),
      backgroundColor: Colors.orange,
      loaderColor: Colors.white,
    );
  }
}
