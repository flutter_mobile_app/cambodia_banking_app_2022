import 'dart:convert';

import 'package:cambodia_banking_app/apis/api.dart';
import 'package:cambodia_banking_app/models/auth/login_req.dart';
import 'package:cambodia_banking_app/models/auth/login_res.dart';
import 'package:cambodia_banking_app/models/base_res.dart';
import 'package:cambodia_banking_app/models/category/Category.dart';
import 'package:cambodia_banking_app/models/category/CategoryByIdRes.dart';
import 'package:cambodia_banking_app/models/category/CategoryRes.dart';
import 'package:cambodia_banking_app/models/openaccount/OpenAccountReq.dart';
import 'package:cambodia_banking_app/models/openaccount/OpenAccountRes.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

class ApiManager extends Api {
  Future<LoginRes> login(String phone, password) async {
    LoginRes res = LoginRes();
    LoginReq req = LoginReq();
    req.phoneNumber = phone;
    req.password = password;
    var url = Uri.parse(baseUrl + authTokenUrl);
    var response = await http.post(url,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(req.toJson()));
    if (response.statusCode != 200) {
      throw ("Your username and password incorrect!");
    }
    if (response.statusCode == 200) {
      Map map = jsonDecode(response.body);
      res = LoginRes.formJson(map);
    }
    return res;
  }

  Future<OpenAccountRes> openAccount(OpenAccountReq req) async {
    OpenAccountRes res = OpenAccountRes();
    var url = Uri.parse(baseUrl + openAccountUrl);
    var response = await http.post(url,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(req.toJson()));
    if (response.statusCode == 200 || response.statusCode == 400) {
      final Map json = jsonDecode(response.body);
      res = OpenAccountRes.fromJson(json);
      if (res.code == "400") {
        throw ("${res.data}");
      }
    }
    return res;
  }

  Future<CategoryRes> getAllCategory() async {
    CategoryRes res = CategoryRes();
    var url = Uri.parse(baseUrl + getCategoryUrl);
    var response = await http.get(url, headers: headerWithToken(getToken()));
    if (response.statusCode == 200) {
      final Map map = json.decode(utf8.decode(response.bodyBytes));
      res = CategoryRes.fromJson(map);
    }
    return res;
  }

  Future<BaseRes> createCategory(Category req) async {
    BaseRes res = BaseRes();
    try {
      var url = Uri.parse(baseUrl + createCategoryUrl);
      var response = await http.post(url,
          headers: headerWithToken(getToken()), body: jsonEncode(req.toJson()));
      if (response.statusCode == 200) {
        final Map map = json.decode(utf8.decode(response.bodyBytes));
        res = BaseRes.fromJson(map);
      }
    } catch (e) {
      throw ("${e}");
    }
    return res;
  }

  Future<BaseRes> updateCategory(Category req) async {
    BaseRes res = BaseRes();
    try {
      var url = Uri.parse(baseUrl + updateCategoryUrl);
      var response = await http.post(url,
          headers: headerWithToken(getToken()), body: jsonEncode(req.toJson()));
      if (response.statusCode == 200) {
        final Map map = json.decode(utf8.decode(response.bodyBytes));
        res = BaseRes.fromJson(map);
      }
    } catch (e) {
      throw ("${e}");
    }
    return res;
  }

  Future<CategoryByIdRes> getCategoryById(int id) async {
    CategoryByIdRes res = CategoryByIdRes();
    try {
      var url = Uri.parse(baseUrl + getCategoryByIdUrl + "${id}");
      var response = await http.get(url, headers: headerWithToken(getToken()));
      if (response.statusCode == 200) {
        final Map map = json.decode(utf8.decode(response.bodyBytes));
        res = CategoryByIdRes.fromJson(map);
      }
    } catch (e) {
      throw ("${e}");
    }
    return res;
  }

  String getToken() {
    final localStorage = LocalStorage("TOKEN_APP");
    var accessToken = localStorage.getItem("ACCESS_TOKEN");
    LoginRes loginRes = LoginRes.formJson(accessToken);
    return loginRes.accessToken!;
  }
}
