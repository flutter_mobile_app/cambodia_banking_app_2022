abstract class Api {
  String baseUrl = "http://143.198.88.147:20022";
  String authTokenUrl = "/api/oauth/token";
  String openAccountUrl = "/api/oauth/open/account";
  String getCategoryUrl = "/api/app/category/list";
  String createCategoryUrl = "/api/app/category/create";
  String getCategoryByIdUrl = "/api/app/category/";
  String updateCategoryUrl = "/api/app/category/update";

  Map<String, String> headerWithToken(String token) {
    Map<String, String> map = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    return map;
  }
}
