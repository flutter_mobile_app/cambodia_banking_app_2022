import 'package:cambodia_banking_app/apis/api_manager.dart';
import 'package:cambodia_banking_app/models/openaccount/OpenAccountReq.dart';
import 'package:cambodia_banking_app/widgets/button_custom_widget.dart';
import 'package:cambodia_banking_app/widgets/input_text_custom_widget.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  var _lastNameController = TextEditingController();
  var _firstNameController = TextEditingController();
  var _usernameNameController = TextEditingController();
  var _emailController = TextEditingController();
  var _phoneController = TextEditingController();
  var _nationalIdController = TextEditingController();
  var _dobController = TextEditingController();
  var _passwordController = TextEditingController();
  var _confirmPasswordController = TextEditingController();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Open Account"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                InputTextCustomWidget(
                  controller: _firstNameController,
                  name: "First Name",
                  icon: Icon(Icons.edit),
                ),
                InputTextCustomWidget(
                  controller: _lastNameController,
                  name: "Last Name",
                  icon: Icon(Icons.edit),
                ),
                InputTextCustomWidget(
                  controller: _usernameNameController,
                  name: "Username",
                  icon: Icon(Icons.supervised_user_circle_outlined),
                ),
                InputTextCustomWidget(
                  controller: _phoneController,
                  name: "Phone",
                  icon: Icon(Icons.edit),
                ),
                InputTextCustomWidget(
                  controller: _emailController,
                  name: "Email",
                  icon: Icon(Icons.alternate_email),
                ),
                InputTextCustomWidget(
                  controller: _dobController,
                  name: "Date of Birt",
                  icon: Icon(Icons.date_range),
                ),
                InputTextCustomWidget(
                  controller: _nationalIdController,
                  name: "National ID",
                  icon: Icon(Icons.alternate_email),
                ),
                InputTextCustomWidget(
                  controller: _passwordController,
                  name: "Password",
                  icon: Icon(Icons.password),
                ),
                InputTextCustomWidget(
                  controller: _confirmPasswordController,
                  name: "Confirm Password",
                  icon: Icon(Icons.password),
                ),
                ButtonCustomWidget(
                  onClick: () {
                    onOpenAccount();
                  },
                  name: "Open Account",
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  onOpenAccount() {
    if (_formKey.currentState!.validate()) {
      setState(() {
        _isLoading = true;
      });
      OpenAccountReq req = OpenAccountReq();
      req.firstName = _firstNameController.text;
      req.lastName = _lastNameController.text;
      req.phoneNumber = _phoneController.text;
      req.nationalId = _nationalIdController.text;
      req.dob = _dobController.text;
      req.email = _emailController.text;
      req.username = _usernameNameController.text;
      req.password = _passwordController.text;
      req.confirmPassword = _confirmPasswordController.text;
      ApiManager().openAccount(req).then((value) {
        setState(() {
          _isLoading = false;
        });
        final snackBar = SnackBar(content: Text('${value.data}'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.of(context).pushNamedAndRemoveUntil("/", (route) => false);
      }).catchError((onError) {
        setState(() {
          _isLoading = false;
        });
        final snackBar = SnackBar(content: Text('${onError}'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }
  }
}
