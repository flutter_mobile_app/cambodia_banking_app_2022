import 'package:cambodia_banking_app/models/user.dart';
import 'package:flutter/material.dart';

class OtpScreen extends StatefulWidget {
  final User user;
  const OtpScreen({Key? key, required this.user}) : super(key: key);

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  final _formKey = GlobalKey<FormState>();
  var _otpController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              margin: EdgeInsets.only(top: 60, left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Confirm OTP",
                    style: TextStyle(color: Colors.black54, fontSize: 32),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text("Please don’t share your OTP to other people"),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: _otpController,
                    validator: (val) {
                      if (val == null || val.isEmpty) {
                        return "Please enter Username or Phone or Account";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.phone,
                          color: Colors.orange,
                        ),
                        border: OutlineInputBorder(),
                        label: Text("Input OTP"),
                        hintText: "Input OTP"),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: () {
                      if (_formKey.currentState!.validate()) {}
                    },
                    child: Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                        child: Text(
                          "Confirm",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text("No acccount yet?"),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
