import 'package:cambodia_banking_app/apis/api_manager.dart';
import 'package:cambodia_banking_app/screens/authentication/register_screen.dart';
import 'package:cambodia_banking_app/screens/languge/language_screen.dart';
import 'package:cambodia_banking_app/widgets/button_custom_widget.dart';
import 'package:cambodia_banking_app/widgets/button_loading_custom_widget.dart';
import 'package:cambodia_banking_app/widgets/input_text_custom_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  var _phoneController = TextEditingController();
  var _passwordController = TextEditingController();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("login".tr()),
        actions: [
          IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> LanguageScreen())).then((value){
              if (value == "KM") {
                context.setLocale(Locale('km', 'KM'));
              }
              if (value == "EN") {
                context.setLocale(Locale('en', 'EN'));
              }
            });
          }, icon: Icon(Icons.language))
        ],
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              margin: EdgeInsets.only(top: 60, left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Login",
                    style: TextStyle(color: Colors.black54, fontSize: 32),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                      "You can login your account banking\nwith phone number and username "),
                  SizedBox(
                    height: 10,
                  ),
                  InputTextCustomWidget(
                    controller: _phoneController,
                    name: "Phone or Account No",
                    icon: Icon(Icons.phone),
                  ),
                  InputTextCustomWidget(
                    password: true,
                    controller: _passwordController,
                    name: "Password",
                    icon: Icon(Icons.password),
                  ),
                  Text("Forget Password?"),
                  SizedBox(
                    height: 10,
                  ),
                  _isLoading == true
                      ? ButtonLoadingCustomWidget()
                      : ButtonCustomWidget(
                          onClick: () {
                            login();
                          },
                          name: "Login",
                        ),
                  SizedBox(
                    height: 10,
                  ),
                  Text("No acccount yet?"),
                  SizedBox(
                    height: 10,
                  ),
                  ButtonCustomWidget(
                    onClick: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>RegisterScreen()));
                    },
                    name: "Open Account",
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  login() {
    final localStorage = LocalStorage("TOKEN_APP");
    if (_formKey.currentState!.validate()) {
      setState(() {
        _isLoading = true;
      });
      String phone = _phoneController.text;
      String password = _passwordController.text;
      print("PASSWORD: ${password} PHONE : ${phone}");
      ApiManager().login(phone, password).then((value) {
        // Success
        setState(() {
          _isLoading = false;
        });
        localStorage.setItem("ACCESS_TOKEN", value.toJson());
        Navigator.of(context).pushNamedAndRemoveUntil("/", (route) => false);
      }).catchError((onError) {
        // Error
        setState(() {
          _isLoading = false;
        });
        final snackBar = SnackBar(content: Text('${onError}'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }
  }
}
