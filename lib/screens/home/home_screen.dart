import 'package:cached_network_image/cached_network_image.dart';
import 'package:cambodia_banking_app/apis/api_manager.dart';
import 'package:cambodia_banking_app/models/category/Category.dart';
import 'package:cambodia_banking_app/screens/category/list_category_screen.dart';
import 'package:cambodia_banking_app/screens/languge/language_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Category> categoryList = [];
  bool _isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    getAllCategory();
    super.initState();
  }

  void getAllCategory() {
    setState(() {
      _isLoading = true;
    });
    ApiManager().getAllCategory().then((value) {
      setState(() {
        categoryList = value.data!.menuList!;
        _isLoading = false;
      });
    }).catchError((onError) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> ListCategoryScreen()));
              },
              leading: Icon(Icons.category),
              title: Text("List Category"),
              trailing: Icon(Icons.more_horiz),
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Text("Home Screen"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LanguageScreen())).then((value) {
                  if (value == "KM") {
                    context.setLocale(Locale('km', 'KM'));
                  }
                  if (value == "EN") {
                    context.setLocale(Locale('en', 'EN'));
                  }
                });
              },
              icon: Icon(Icons.language))
        ],
      ),
      body: _isLoading == true
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(5),
                child: GridView.builder(
                  shrinkWrap: true,
                  itemCount: categoryList.length,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    var category = categoryList[index];
                    return InkWell(
                      onTap: () {},
                      child: Container(
                        margin: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              CachedNetworkImage(
                                width: 40,
                                imageUrl: "${category.imageUrl}",
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                "khmer".tr() == "Khmer"
                                    ? "${category.name}"
                                    : "${category.nameKh}",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
          ),
    );
  }
}
