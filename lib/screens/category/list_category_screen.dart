import 'package:cambodia_banking_app/apis/api_manager.dart';
import 'package:cambodia_banking_app/models/category/Category.dart';
import 'package:cambodia_banking_app/screens/category/form_category_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ListCategoryScreen extends StatefulWidget {
  const ListCategoryScreen({Key? key}) : super(key: key);

  @override
  State<ListCategoryScreen> createState() => _ListCategoryState();
}

class _ListCategoryState extends State<ListCategoryScreen> {
  List<Category> categoryList = [];
  bool _isLoading = false;

  @override
  void initState() {
    getAllCategory();
    super.initState();
  }

  getAllCategory() {
    setState(() {
      _isLoading = true;
    });
    ApiManager().getAllCategory().then((value) {
      setState(() {
        _isLoading = false;
        categoryList = value.data!.menuList!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("list_category".tr()),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FormCategoryScreen())).then(
                (value) {
                  if (value == true) {
                    getAllCategory();
                  }
                },
              );
            },
            icon: Icon(Icons.add),
          )
        ],
      ),
      body: _isLoading == true
          ? Center(child: CircularProgressIndicator())
          : Container(
              padding: EdgeInsets.all(10),
              child: ListView.builder(
                  itemCount: categoryList.length,
                  itemBuilder: (BuildContext context, index) {
                    var category = categoryList[index];
                    return ListTile(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FormCategoryScreen(
                                      category: category,
                                    ))).then((value) {
                          if (value == true) {
                            getAllCategory();
                          }
                        });
                      },
                      leading: Icon(Icons.category),
                      title: Text("${category.name}"),
                      subtitle: Text("${category.nameKh}"),
                      trailing: IconButton(onPressed: (){}, icon: Icon(Icons.delete,color: Colors.red,),),
                    );
                  }),
            ),
    );
  }
}
