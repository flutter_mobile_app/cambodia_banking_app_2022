import 'package:cambodia_banking_app/apis/api_manager.dart';
import 'package:cambodia_banking_app/models/category/Category.dart';
import 'package:cambodia_banking_app/widgets/button_custom_widget.dart';
import 'package:cambodia_banking_app/widgets/input_text_custom_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class FormCategoryScreen extends StatefulWidget {
  Category? category;

  FormCategoryScreen({Key? key, this.category}) : super(key: key);

  @override
  State<FormCategoryScreen> createState() => _FormCategoryScreenState();
}

class _FormCategoryScreenState extends State<FormCategoryScreen> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _nameKhController = TextEditingController();
  final _indexController = TextEditingController();
  final _categoryCodeKhController = TextEditingController();
  final _routeController = TextEditingController();
  bool _isLoading = false;
  Category category = Category();

  @override
  void initState() {
    // TODO: implement initState
    if (widget.category != null) {
      getCategoryById(widget.category!.id!);
    }
    super.initState();
  }

  getCategoryById(int id) {
    setState(() {
      _isLoading = true;
    });
    ApiManager().getCategoryById(id).then((value) {
      setState(() {
        _isLoading = false;
      });
      category = value.data!;
      _nameController.text = category.name!;
      _nameKhController.text = category.nameKh!;
      _indexController.text = category.index!.toString();
      _categoryCodeKhController.text = category.categoryCode!;
      _routeController.text = category.route!;
    }).catchError((onError) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category == null
            ? "create_category".tr()
            : "update_category".tr()),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: _isLoading == true
              ? Center(child: CircularProgressIndicator())
              : Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      InputTextCustomWidget(
                        controller: _nameController,
                        name: "name".tr(),
                        icon: Icon(Icons.edit),
                      ),
                      InputTextCustomWidget(
                        controller: _nameKhController,
                        name: "name_kh".tr(),
                        icon: Icon(Icons.edit),
                      ),
                      InputTextCustomWidget(
                        controller: _indexController,
                        name: "index".tr(),
                        icon: Icon(Icons.edit),
                      ),
                      InputTextCustomWidget(
                        controller: _routeController,
                        name: "route".tr(),
                        icon: Icon(Icons.edit),
                      ),
                      InputTextCustomWidget(
                        controller: _categoryCodeKhController,
                        name: "code".tr(),
                        icon: Icon(Icons.edit),
                      ),
                      ButtonCustomWidget(
                        onClick: () {
                          if(widget.category==null){
                            create();
                          }else{
                            update();
                          }

                        },
                        name: widget.category == null
                            ? "create".tr()
                            : "update".tr(),
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  create() {
    if (_formKey.currentState!.validate()) {
      setState(() {
        _isLoading = true;
      });
      Category req = Category();
      req.id = 0;
      req.name = _nameController.text;
      req.nameKh = _nameKhController.text;
      req.index = int.parse(_indexController.text);
      req.categoryCode = _categoryCodeKhController.text;
      req.route = _routeController.text;
      req.imageUrl = null;
      ApiManager().createCategory(req).then((value) {
        setState(() {
          _isLoading = false;
        });
        final snackBar = SnackBar(content: Text('${value.message}'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.pop(context, true);
      }).catchError((onError) {
        setState(() {
          _isLoading = false;
        });
        final snackBar = SnackBar(content: Text('${onError}'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }
  }

  update() {
    if (_formKey.currentState!.validate()) {
      setState(() {
        _isLoading = true;
      });
      category.name = _nameController.text;
      category.nameKh = _nameKhController.text;
      category.index = int.parse(_indexController.text);
      category.categoryCode = _categoryCodeKhController.text;
      category.route = _routeController.text;
      category.imageUrl = null;
      ApiManager().updateCategory(category).then((value) {
        setState(() {
          _isLoading = false;
        });
        final snackBar = SnackBar(content: Text('${value.message}'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.pop(context, true);
      }).catchError((onError) {
        setState(() {
          _isLoading = false;
        });
        final snackBar = SnackBar(content: Text('${onError}'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }
  }
}
