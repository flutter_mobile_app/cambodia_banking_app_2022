import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class LanguageScreen extends StatefulWidget {
  const LanguageScreen({Key? key}) : super(key: key);

  @override
  State<LanguageScreen> createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("change_language".tr()),
      ),
      body:  Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Card(
              child: ListTile(
                onTap: (){
                  context.setLocale(Locale('km', 'KM'));
                  Navigator.pop(context,"KM");
                },
                title: Text("khmer".tr()),
                trailing: "khmer".tr() == "Khmer" ? Text("") : Icon(Icons.check) ,
              ),
            ),
            Card(
              child: ListTile(
                onTap: (){
                  context.setLocale(Locale('en', 'EN'));
                  Navigator.pop(context,"EN");
                },
                title: Text("english".tr()),
                trailing: "khmer".tr() == "Khmer" ?  Icon(Icons.check) : Text(""),
              ),
            )
          ],
        ),
      ),
    );

  }
}
