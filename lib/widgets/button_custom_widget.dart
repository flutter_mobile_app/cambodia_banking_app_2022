import 'package:flutter/material.dart';

class ButtonCustomWidget extends StatefulWidget {
  VoidCallback? onClick;
  String? name;

  ButtonCustomWidget({Key? key, this.onClick, this.name}) : super(key: key);

  @override
  State<ButtonCustomWidget> createState() => _ButtonCustomWidgetState();
}

class _ButtonCustomWidgetState extends State<ButtonCustomWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onClick,
      child: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
            color: Colors.orange, borderRadius: BorderRadius.circular(10)),
        child: Center(
          child: Text(
            "${widget.name ?? ""}",
            style: TextStyle(color: Colors.white),
          ),
        ),

      ),
    );
  }
}
