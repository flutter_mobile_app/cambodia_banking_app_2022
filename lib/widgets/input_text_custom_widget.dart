import 'package:flutter/material.dart';

class InputTextCustomWidget extends StatefulWidget {
  TextEditingController? controller;
  String? name;
  Icon? icon;
  bool? password;

  InputTextCustomWidget(
      {Key? key, this.controller, this.name, this.icon, this.password})
      : super(key: key);

  @override
  State<InputTextCustomWidget> createState() => _InputTextCustomWidgetState();
}

class _InputTextCustomWidgetState extends State<InputTextCustomWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        obscureText: widget.password ?? false,
        controller: widget.controller,
        validator: (val) {
          if (val == null || val.isEmpty) {
            return "Please enter ${widget.name}";
          }
          return null;
        },
        decoration: InputDecoration(
            prefixIcon: widget.icon,
            border: OutlineInputBorder(),
            label: Text(" ${widget.name}"),
            hintText: " ${widget.name}"),
      ),
    );
  }
}
