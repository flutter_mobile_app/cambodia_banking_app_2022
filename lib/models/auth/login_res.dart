class LoginRes{
  String? accessToken;
  String? refreshToken;
  String? tokenType;
  int? expiresIn;
  LoginRes({this.accessToken,this.refreshToken, this.tokenType, this.expiresIn});

  LoginRes.formJson(Map<dynamic,dynamic> map){
    accessToken = map["accessToken"];
    refreshToken = map["refreshToken"];
    tokenType = map["tokenType"];
    expiresIn = map["expiresIn"];

  }

  Map<dynamic, dynamic> toJson(){
    Map<dynamic,dynamic> map ={};
    map["accessToken"]=this.accessToken;
    map["refreshToken"]=this.refreshToken;
    map["tokenType"]=this.tokenType;
    map["expiresIn"]=this.expiresIn;
    return map;
  }
}