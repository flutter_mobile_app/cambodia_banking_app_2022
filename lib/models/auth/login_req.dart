class LoginReq {
  String? phoneNumber;
  String? password;

  LoginReq({this.phoneNumber, this.password});

  Map<dynamic, dynamic> toJson() {
    Map<dynamic, dynamic> map = {};
    map["phoneNumber"] = this.phoneNumber;
    map["password"] = this.password;
    return map;
  }

}
