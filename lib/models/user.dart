class User {
  String? phone;
  String? token;
  String? password;
  User({this.phone, this.password, this.token});

  toJsonData() {
    Map<String, dynamic> map = new Map();
    map["phone"] = this.phone;
    map["token"] = this.token;
    map["password"] = this.password;
    return map;
  }
}
