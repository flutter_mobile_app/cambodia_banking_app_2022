import 'package:cambodia_banking_app/models/category/Category.dart';

class CategoryByIdRes {
  CategoryByIdRes({
      this.message, 
      this.code, 
      this.data,});

  CategoryByIdRes.fromJson(dynamic json) {
    message = json['message'];
    code = json['code'];
    data = json['data'] != null ? Category.fromJson(json['data']) : null;
  }
  String? message;
  String? code;
  Category? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    map['code'] = code;
    if (data != null) {
      map['data'] = data!.toJson();
    }
    return map;
  }

}