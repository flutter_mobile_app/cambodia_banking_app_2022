import 'package:cambodia_banking_app/app/splash_screen_app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(
    EasyLocalization(
        supportedLocales: [Locale('en', 'EN'), Locale('km', 'KM')],
        path: 'assets/langs', // <-- change the path of the translation files
        fallbackLocale: Locale('km', 'KM'),
        child: const MyApp()
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'Cambodia Banking App',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const SplashScreenApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}